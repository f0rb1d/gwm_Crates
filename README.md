![](https://cdn.discordapp.com/attachments/317517981745807361/352965090590326784/unknown.png)  
  
## [Gallery](https://ore.spongepowered.org/GWM/GWMCrates/pages/Gallery) | [Getting Started](https://gitlab.com/GreWeMa/gwm_Crates/wikis/Getting-started) | [Wiki](https://gitlab.com/GreWeMa/gwm_Crates/wikis) | [Forum](https://forums.spongepowered.org/t/16379) | [Discord](https://discord.gg/ZBzWAsS) | [Translations](https://gitlab.com/GreWeMa/gwm_Crates/tree/master/src/main/resources/translations) | [Ore Page](https://ore.spongepowered.org/GWM/GWMCrates)

**GWM_Crates** - a plugin for minecraft servers that uses Sponge or SpongeForge. With this plugin you are able to add as many crates as you want, and you can configure them as you like.  
You can read our documentation about GWM_Crates and it's configuration on our wiki. [Click here](https://gitlab.com/GreWeMa/gwm_Crates/wikis)

For discussions we have [forum thread](https://forums.spongepowered.org/t/16379) and our [discord server](https://discord.gg/Y94eyaX).

Also, our plugin has very good API for developers, which allows you to customize the plugin even more!
